# Construct a common base image for creating python wheels and the final image
FROM docker.io/python:3.12.7-alpine3.20 AS runtime_base

# Setup user
RUN addgroup -g 500 -S hunter2 \
 && adduser -h /opt/hunter2 -s /sbin/nologin -G hunter2 -S -u 500 hunter2
WORKDIR /opt/hunter2/src


# Build image with all the python dependancies.
FROM runtime_base AS python_build

RUN --mount=type=cache,target=/var/cache/apk apk add \
    gcc \
    libffi-dev \
    musl-dev

# Suppress pip version warning, we're keeping the version from the docker base image
ARG PIP_DISABLE_PIP_VERSION_CHECK=1

ENV PATH "/root/.local/bin:${PATH}"
ARG poetry_installer_version=9b64f71d730d7be00e204b96a095b38af9e909e3
ARG poetry_version=1.8.1
RUN wget -q -O - https://raw.githubusercontent.com/python-poetry/install.python-poetry.org/${poetry_installer_version}/install-poetry.py | python3 - --version ${poetry_version} \
 && poetry config virtualenvs.create false \
 && python -m venv /opt/hunter2/venv

ARG dev_flag=" --only main"
COPY poetry.lock pyproject.toml /opt/hunter2/src/
RUN --mount=type=cache,target=/root/.cache/pip \
    . /opt/hunter2/venv/bin/activate \
 && poetry install${dev_flag} --no-root


# Build the final image
FROM runtime_base

# Copy in the requried components from the previous build stages
COPY --from=python_build /opt/hunter2/venv /opt/hunter2/venv
COPY . .

USER hunter2

ENTRYPOINT ["/opt/hunter2/venv/bin/python", "bot.py"]
