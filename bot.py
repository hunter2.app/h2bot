# Copyright (C) 2021 The Hunter2 Contributors.
#
# This file is part of Hunter2 Bot.
#
# Hunter2 Bot is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
import itertools
import os
from uuid import UUID
import re

import aiohttp
import discord
import structlog
from aiohttp import ClientError
from fuzzywuzzy import process as fuzzy
from discord.ext import commands

from utils import config
from cogs.finishers import Finishers
from utils.discord.category import get_category_by_name
from utils.censor import censor_message_if_public
from utils.discord.text_channel import get_text_channel_by_name
from utils.discord.role import ensure_role, get_role_by_name, role_name_for_team, check_member_role
from utils.discord.voice_channel import get_voice_channel_by_name

log = structlog.get_logger(__name__)

# Assumptions:
# Team names don't change (not currently supported by H2, but would result in "old" role names)

intents = discord.Intents(messages=True, members=True, guilds=True, dm_messages=True,
                          guild_messages=True, guild_reactions=True, reactions=True,
                          message_content=True)
hc = commands.DefaultHelpCommand(width=1000, no_category="Commands")


class CustomBot(commands.Bot):
    async def setup_hook(self):
        await self.add_cog(Finishers(self, config.HUNTER2_URL, config.HUNTER2_TOKEN))


bot = CustomBot(command_prefix='!', description="Your friendly puzzle hunt bot", intents=intents, help_command=hc)

# Import all plugins
if os.path.isdir('./plugins'):
    for filename in os.listdir('./plugins'):
        if filename.endswith('.py'):
            bot.load_extension(f'plugins.{filename[:-3]}')


def _log_params(ctx):
    log_params = {
        'channel_type': str(ctx.channel.type),
        'command': ctx.command.name,
        'user': ctx.message.author.name,
    }
    if hasattr(ctx.message.channel, 'name'):
        log_params['channel'] = ctx.channel.name
    return log_params


@bot.before_invoke
async def before_invoke(ctx):
    log.info("Received command", **_log_params(ctx))


@bot.after_invoke
async def after_invoke(ctx):
    log.info("Completed command", **_log_params(ctx))


@bot.event
async def on_ready():
    log.info('Logged in', user=bot.user.name, id=bot.user.id)
    # Import all plugins
    if os.path.isdir('./plugins'):
        for filename in os.listdir('./plugins'):
            if filename.endswith('.py'):
                log.info('Loading plugin', plugin=filename[:-3])
                await bot.load_extension(f'plugins.{filename[:-3]}')


@bot.command(name='feedback', help="Provide general feedback (use !stuck for anything puzzle specific)")
async def feedback(ctx, *feedback_words):
    feedback = ' '.join(feedback_words)
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = message.author
    member = await guild.fetch_member(user.id)

    if not feedback:
        feedback_text = ("Feedback link is: {}".format(config.FEEDBACK_LINK) if config.FEEDBACK_LINK else
                         "The feedback form is not available yet - please check back later or DM an admin")
        await user.send(content="Your feedback is welcome! " + feedback_text)
        return
    feedbackchan = list(chan for chan in guild.channels if chan.name == config.ADMIN_FEEDBACK_CHANNEL)[0]

    member_roles = list(role for role in member.roles if role.name.startswith("team-"))

    team_text = ""
    guessed_teamchat = ""

    if member_roles:
        team_text = "from team '{}' ".format(member_roles[0].name[5:])
        guessed_teamchat = "teamchat-{}".format(member_roles[0].name[5:].lower().replace(" ", "-"))
        # [5:] will strip "team-" from the beginning of a string

    if message.guild:
        # If a user !feedbacks in a team channel, link directly to the message
        reply_link = message.jump_url
    else:
        # If a user !feedbacks in a DM, guess the team channel
        channels = guild.channels
        reply_link = ""
        for chan in channels:
            if chan.name == guessed_teamchat:
                reply_link = chan.mention

    await feedbackchan.send(content="User {} {}sends feedback: {}. {}".format(user.mention, team_text, feedback[:1500], reply_link))

    response_message = "Thank you, {}. Your feedback is appreciated!".format(user)
    if await censor_message_if_public(ctx):
        await user.send(response_message)
    else:
        await ctx.send(response_message, reference=ctx.message)


@bot.command(name='stuck', help="Informs the admins you are stuck on a puzzle. Please only use this in a PM with the bot or a team chat")
async def stuck(ctx, *problem_words):
    problem = ' '.join(problem_words)
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = message.author
    member = await guild.fetch_member(user.id)

    if not problem:
        problem = "No problem specified"
    stuckchan = list(chan for chan in guild.channels if chan.name == config.ADMIN_STUCK_CHANNEL)[0]

    member_roles = list(role for role in member.roles if role.name.startswith("team-"))

    team_text = ""
    guessed_teamchat = ""

    if member_roles:
        team_text = "from team '{}' ".format(member_roles[0].name[5:])
        guessed_teamchat = "teamchat-{}".format(member_roles[0].name[5:].lower().replace(" ", "-"))
        # [5:] will strip "team-" from the beginning of a string

    if message.guild:
        # If a user !stucks in a team channel, link directly to the message
        reply_link = message.jump_url
    else:
        # If a user !stucks in a DM, guess the team channel
        channels = guild.channels
        reply_link = ""
        for chan in channels:
            if chan.name == guessed_teamchat:
                reply_link = chan.mention

    await stuckchan.send(content="User {} {}is stuck with message: {}. {}".format(user.mention, team_text, problem[:1500], reply_link))

    response_message = "Thank you, {}. The admins have been informed of your woes".format(user)
    if await censor_message_if_public(ctx):
        await user.send(response_message)
    else:
        await ctx.send(response_message, reference=ctx.message)


@bot.command(name='jointeam',
             help="Join your team chats with the ID found on the 'team' page. Please only use this in a PM with the bot",
             aliases=['join', 'joinTeam'])
async def jointeam(ctx, teamid):
    guild = ctx.guild or bot.guilds[0]
    user = ctx.message.author
    member = await guild.fetch_member(user.id)

    await censor_message_if_public(ctx)

    log_params = {
        'user': user.name,
        'token': teamid,
    }

    try:
        UUID(teamid)
    except ValueError:
        await user.send(content="That doesn't look like a valid team join token")
        log.info("Invalid team join token", **log_params)
        return

    member_roles = list(role for role in member.roles if role.name.startswith("team-"))
    if member_roles:
        await user.send(content="You already have joined a discord team channel. Please contact the admins to change team")
        log.info("User already joined to team", **log_params)
        return

    url = '{}/hunt/teaminfo/{}'.format(config.HUNTER2_URL, teamid)
    headers = {'Authorization': "Bearer " + config.HUNTER2_TOKEN}
    try:
        async with ctx.bot.session.get(url, headers=headers) as response:
            if response.status == 404:
                log.warning("No such team", **log_params)
                await user.send(
                    content="There was an error joining your team. "
                            "Please double check the code you fetched from the 'Team' page, and if the problem persists, contact the hunt admins"
                )
                return

            if response.status != 200:
                log.warning("Error fetching team", **log_params, status=response.status)
                await user.send(content="There was an error joining your team. Please contact the hunt admins")
                return

            content = await response.json()

    except ClientError as e:
        log.error("Client error fetching team", **log_params, exc_info=e)
        await user.send(content="There was an error joining your team. Please contact the hunt admins")
        return

    teamname = content['team']['name']
    teamrole = await ensure_role(role_name_for_team(teamname), guild)
    adminrole = get_role_by_name(config.ADMIN_ROLE, guild)
    botrole = guild.me.roles[1]
    # The bot needs permissions to see into the created channels.
    # This assumes the bot only has one role, as it is hard to determine the "bot" role (this could be a config option in future)

    permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=False),
            teamrole: discord.PermissionOverwrite(read_messages=True, manage_messages=True),
            botrole: discord.PermissionOverwrite(read_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }

    category = await createCategoryIfNotExists("Team: {}".format(teamname)[:100], permissions, guild)
    textchan = await createTextChannelIfNotExists("teamchat-{}".format(teamname)[:100], category, guild)
    voicechan = await createVoiceChannelIfNotExists("teamvoice-{}".format(teamname)[:100], category, guild)

    await member.add_roles(teamrole)
    await user.send(content="You have been joined to your team channels: {} and {}".format(textchan.mention, voicechan.mention))


@bot.command(name='meme', help="Submit a meme for approval. Please only use this in a PM with the bot or a team chat", aliases=['memes'])
async def meme(ctx):
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = ctx.message.author

    memechan = get_text_channel_by_name(config.ADMIN_MEME_CHANNEL, guild)

    files = []
    for attachment in message.attachments:
        fl = await attachment.to_file()
        files.append(fl)
    memecontent = re.sub(r'\!memes?', '', message.content)
    message = await memechan.send(content="Submitted by {}. {}".format(user.mention, memecontent), files=files)

    response_message = "Your meme has been submitted"
    if await censor_message_if_public(ctx):
        await user.send(content=response_message)
    else:
        await ctx.send(content=response_message, reference=ctx.message)

    await message.add_reaction('\N{THUMBS UP SIGN}')
    for i in range(0, len(config.EPISODES)):
        await message.add_reaction(u"{}\N{VARIATION SELECTOR-16}\N{COMBINING ENCLOSING KEYCAP}".format(i+1))
    await message.add_reaction('\N{THUMBS DOWN SIGN}')


@bot.command(name='serversetup', help="(Admin Only) Setup the required admin channels and permissions", hidden=True)
async def serversetup(ctx):
    guild = ctx.guild or bot.guilds[0]
    user = ctx.message.author
    if type(user) != discord.Member:
        await user.send(content="Please ensure this is run from within the discord server you wish to configure")
        return
    if not await check_member_role(user, [config.DISCORD_ADMIN_ROLE], guild) and user != guild.owner:
        log.warning("Server setup permission denied", user=user.name)
        await ctx.send("You need to be server owner or admin to use this command")
        return

    for role in config.ROLES_TO_CREATE:
        await ensure_role(role, guild)

    adminrole = get_role_by_name(config.ADMIN_ROLE, guild)
    botrole = guild.me.roles[1]

# PUBLIC OBJECTS
    public_permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=True, mention_everyone=False),
            botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }
    publiccat = await createCategoryIfNotExists(config.PUBLIC_CATEGORY, public_permissions, guild)
    for chan in config.PUBLIC_CHANNELS:
        await createTextChannelIfNotExists(chan, publiccat, guild, stripSpecial=False)

    for chan in config.PUBLIC_VOICE_CHANNELS:
        await createVoiceChannelIfNotExists(chan, publiccat, guild)

# ANNOUNCE OBJECTS
    announce_permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=True, send_messages=False),
            botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }
    for chan in config.ANNOUNCE_CHANNELS:
        await createTextChannelIfNotExists(chan, publiccat, guild, permissions=announce_permissions, stripSpecial=False)


# ADMIN OBJECTS
    admin_permissions = {
            guild.default_role: discord.PermissionOverwrite(read_messages=False),
            botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
            adminrole: discord.PermissionOverwrite(read_messages=True)
            }
    admincat = await createCategoryIfNotExists(config.ADMIN_CATEGORY, admin_permissions, guild)

    for chan in config.ADMIN_CHANNELS:
        await createTextChannelIfNotExists(chan, admincat, guild, stripSpecial=False)

    for chan in config.ADMIN_VOICE_CHANNELS:
        await createVoiceChannelIfNotExists(chan, admincat, guild)

# FINISHERS OBJECTS
    for episode in config.EPISODES:
        role = await ensure_role("{}-finishers".format(episode), guild)

        finish_permissions = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                role: discord.PermissionOverwrite(read_messages=True),
                botrole: discord.PermissionOverwrite(read_messages=True, send_messages=True),
                adminrole: discord.PermissionOverwrite(read_messages=True)
                }
        finishmeme_permissions = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                role: discord.PermissionOverwrite(read_messages=True, send_messages=False),
                botrole: discord.PermissionOverwrite(read_messages=True),
                adminrole: discord.PermissionOverwrite(read_messages=True)
                }
        finishcat = await createCategoryIfNotExists(episode, finish_permissions, guild)

        await createTextChannelIfNotExists("{}-finishers".format(episode), finishcat, guild, stripSpecial=False)
        await createTextChannelIfNotExists("{}-memes".format(episode), finishcat, guild, permissions=finishmeme_permissions, stripSpecial=False)
        await createVoiceChannelIfNotExists("{}-finishers".format(episode), finishcat, guild)


@bot.command(name='springclean', help="(Admin Only) Remove all team/finishers channels, categories and roles", hidden=True)
async def springclean(ctx, confirmation=0):
    guild = ctx.guild or bot.guilds[0]
    user = ctx.message.author

    if type(user) != discord.Member:
        await user.send(content="Please ensure this is run from within the discord server you wish to configure")
        return
    if not await check_member_role(user, [config.DISCORD_ADMIN_ROLE], guild) and user != guild.owner:
        log.warning("Spring clean permission denied", user=user.name)
        await ctx.send("You need to be server owner or admin to use this command")
        return

    def _objects_and_names_with_exclusions(input, exclusions):
        objects = [i for i in input if i.name not in exclusions]
        names = [o.name for o in objects]
        return objects, names

    text_channels, text_channel_names = _objects_and_names_with_exclusions(guild.text_channels, config.PERMANENT_CHANNELS)
    voice_channels, voice_channel_names = _objects_and_names_with_exclusions(guild.voice_channels, config.PERMANENT_VOICE_CHANNELS)
    categories, category_names = _objects_and_names_with_exclusions(guild.categories, config.PERMANENT_CATEGORIES)
    roles, role_names = _objects_and_names_with_exclusions(guild.roles, config.PERMANENT_ROLES + [r.name for r in guild.me.roles])
    log.info(
        "Spring Clean plan",
        text_channels=text_channel_names,
        voice_channels=voice_channel_names,
        categories=category_names,
        roles=role_names,
    )
    if confirmation == guild.id:
        for channel in text_channels:
            await channel.delete()
            await user.send(f'DELETED TEXT CHANNEL: {channel.name}')
            log.info("Deleted text channel", role=channel.name)
        for channel in voice_channels:
            await channel.delete()
            await user.send(f'DELETED VOICE CHANNEL: {channel.name}')
            log.info("Deleted voice channel", role=channel.name)
        for category in categories:
            await category.delete()
            await user.send(f'DELETED CATEGORY: {category.name}')
            log.info("Deleted category", role=category.name)
        for role in roles:
            await role.delete()
            await user.send(f'DELETED ROLE: {role.name}')
            log.info("Deleted role", role=role.name)
        log.info("Calling server setup")
        await serversetup(ctx)
    else:
        await user.send('DRY RUN PLAN')
        await user.send('WOULD DELETE TEXT CHANNELS:')
        for names in itertools.batched(text_channel_names, 10):
            await user.send('\n'.join(names))
        await user.send('WOULD DELETE VOICE CHANNELS:')
        for names in itertools.batched(voice_channel_names, 10):
            await user.send('\n'.join(names))
        await user.send('WOULD DELETE CATEGORIES:')
        for names in itertools.batched(category_names, 10):
            await user.send('\n'.join(names))
        await user.send('WOULD DELETE ROLES:')
        for names in itertools.batched(role_names, 10):
            await user.send('\n'.join(names))


@bot.command(name='teamsearch', help="(Admin Only) Allows the admins to search for a team's chat", aliases=["ts"], hidden=True)
async def teamsearch(ctx, *teamarray):
    teamname = ' '.join(teamarray)
    guild = ctx.guild or bot.guilds[0]
    message = ctx.message
    user = message.author

    if await censor_message_if_public(ctx):
        return

    if not await check_member_role(user, [config.ADMIN_ROLE], guild) and user != guild.owner:
        log.warning("Team Search permission denied", user=user.name)
        await ctx.send("You need to be server owner or admin to use this command")
        return

    categoryList = []
    for category in guild.categories:
        if "Team: " in category.name:
            categoryList.append(category.name[6:])

    catname = fuzzy.extractOne(teamname, categoryList)[0]
    catobj = get_category_by_name("Team: " + catname, guild)
    tc = catobj.text_channels[0]
    vc = catobj.voice_channels[0]
    await ctx.send(tc.mention)
    await ctx.send(vc.mention)


async def createCategoryIfNotExists(category, permissions, guild, force=False):
    create = force
    try:
        cat_obj = get_category_by_name(category, guild)
    except ValueError:
        create = True

    if create:
        log.info("Creating category", category=category)
        cat_obj = await guild.create_category(name=category, overwrites=permissions)
    return cat_obj


async def createTextChannelIfNotExists(channel, category, guild, permissions=None, force=False, stripSpecial=True):
    if stripSpecial:
        channel = channel.lower().replace(" ", "-")
        channel = re.sub(r'[^a-zA-Z0-9_-]', '', channel)

    create = force

    try:
        chan_obj = get_text_channel_by_name(channel, guild)
        if chan_obj.category != category:
            create = True
    except ValueError:
        create = True

    if create:
        log.info("Creating text channel", text_channel=channel)
        if permissions:
            chan_obj = await guild.create_text_channel(channel, category=category, overwrites=permissions)
        else:
            chan_obj = await guild.create_text_channel(channel, category=category)

    return chan_obj


async def createVoiceChannelIfNotExists(channel, category, guild, force=False):
    create = force
    try:
        chan_obj = get_voice_channel_by_name(channel, guild)
    except ValueError:
        create = True

    if create:
        log.info("Creating voice channel", voice_channel=channel)
        chan_obj = await guild.create_voice_channel(channel, category=category)
    return chan_obj


# Meme approval via reactions
@bot.event
async def on_raw_reaction_add(payload):
    channel = bot.get_channel(payload.channel_id)
    guild = bot.get_guild(payload.guild_id)
    user = bot.get_user(payload.user_id)
    emoji = payload.emoji

    if not guild:  # Ignore DMs
        return

    memeadminchan = get_text_channel_by_name(config.ADMIN_MEME_CHANNEL, guild)
    message = await channel.fetch_message(payload.message_id)
    if user == guild.me:  # Ignore the auto-populated emoji for memes
        return
    if message.channel != memeadminchan:  # Only watch meme admin channel
        return
    if guild.me != message.author:  # Ensure it's a meme submitted through the correct method
        return

    reaction = list(reac for reac in message.reactions if str(reac.emoji) == str(emoji))[0]

    if reaction.count != 2:
        return

    success = 0
    if str(emoji) == u"\N{THUMBS UP SIGN}":
        log.info("Meme approved")
        memechan = get_text_channel_by_name(config.MEME_CHANNEL, guild)
        files = []
        for attachment in message.attachments:
            fl = await attachment.to_file()
            files.append(fl)
        await memechan.send(content=message.content, files=files)
        success = 1
    elif str(emoji) == "\N{THUMBS DOWN SIGN}":
        await message.clear_reactions()
        await message.add_reaction(u'\u274C')  # Cross
    else:
        for i in range(0, len(config.EPISODES)):
            if str(emoji) == u"{}\N{VARIATION SELECTOR-16}\N{COMBINING ENCLOSING KEYCAP}".format(i+1):  # Numbers for episodes
                log.info("Episode meme approved", episode=config.EPISODES[i])
                memechan = get_text_channel_by_name("{}-memes".format(config.EPISODES[i]), guild)
                files = []
                for attachment in message.attachments:
                    fl = await attachment.to_file()
                    files.append(fl)
                await memechan.send(content=message.content, files=files)
                success = 1

    if success:
        await message.clear_reactions()
        await message.add_reaction(u'\u2705')  # Tick
    # After the meme is approved or rejected, the automatically populated reactions are removed
    # If a different reaction is needed, it will require 2 people to add the relevant emoji


async def main():
    async with aiohttp.ClientSession() as session:
        async with bot:
            bot.session = session
            await bot.start(config.TOKEN)


if __name__ == '__main__':
    asyncio.run(main())
