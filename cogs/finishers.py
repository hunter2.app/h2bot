import asyncio
import json
import random

import aiohttp
import inflection
import structlog
from discord.ext import commands
from h2api.protocol import update_stream
from h2api.protocol.update_stream import Message

from utils import config
from utils.censor import censor_message_if_public
from utils.discord.category import get_category_by_name
from utils.discord.role import get_role_by_name, role_name_for_team, check_member_role
from utils.discord.text_channel import get_text_channel_by_name

log = structlog.get_logger(__name__)


class Finishers(commands.Cog):
    def __init__(self, bot, url, token):
        self.bot = bot
        self.url = url
        self.token = token
        self.running = False
        self.teams_map = {}
        self.tasks = []

    async def cog_load(self):
        self.running = True
        await self.refresh_teams_map()
        self.tasks.append(self.bot.loop.create_task(self.websocket_connect()))
        self.tasks.append(self.bot.loop.create_task(self.periodic_teams_map_update()))

    async def cog_unload(self):
        self.running = False
        for task in self.tasks:
            await task

    async def periodic_teams_map_update(self):
        while self.running:
            await asyncio.sleep(random.uniform(50, 70))
            try:
                await self.refresh_teams_map()
            except Exception as e:
                log.error('Error refreshing teams map', exc_info=e)

    async def refresh_teams_map(self):
        log.info("Refreshing teams map")
        async with self.bot.session.get(f'{self.url}/hunt/teamlist/', headers={'Authorization': f'Bearer {self.token}'}) as teamlist_rsp:
            if teamlist_rsp.status != 200:
                log.error('Error fetching teams map', status=teamlist_rsp.status)
            teamlist_data = await teamlist_rsp.json()
            self.teams_map = {team['id']: team['name'] for team in teamlist_data['items']}

    async def handle(self, msg):
        try:
            if msg.type != aiohttp.WSMsgType.TEXT:
                log.error("Non-text message from websocket", type=msg.type)
            msg_json = json.loads(msg.data)
            msg_type = inflection.underscore(msg_json['type'])
            try:
                handler = getattr(self, f'handle_{msg_type}')
                await handler(Message.from_struct(msg_json))
            except AttributeError:
                log.error('Received unknown message type', type=msg_type)
        except Exception as e:
            log.error('Error processing websocket message', exc_info=e)

    async def handle_episode_finished(self, msg):
        log.info('Notifying channel of episode completion', episode=msg.episode_id, team=msg.team_id)
        team = self.teams_map[msg.team_id]
        episode_idx = config.EPISODE_IDS.index(msg.episode_id)
        role_name = role_name_for_team(team)
        role = get_role_by_name(role_name, self.bot.guilds[0])
        await self.add_to_finishers(episode_idx, role)

    async def websocket_connect(self):
        await self.bot.wait_until_ready()
        while self.running:
            url = f'{self.url}/ws/hunt/update_stream'
            log.info('Connecting to hunter2 websocket', url=url)
            async with self.bot.session.ws_connect(url) as ws:
                log.info("Requesting episode finishers")
                try:
                    await ws.send_json(update_stream.EpisodeFinishersPlz(api_token=self.token).to_struct())
                except Exception as e:
                    log.error("Error requesting episode finishers", exc_info=e)
                log.info("Listening for episode finishers")
                async for msg in ws:
                    await self.handle(msg)
                    if not self.running:
                        break

    async def add_to_finishers(self, episode_idx, role):
        category = get_category_by_name(config.EPISODES[episode_idx], self.bot.guilds[0])
        if category.permissions_for(role).read_messages:
            # We've already done this team, let's not spam them
            return
        # We need to ensure we lock down the memes channel before we open up the category to avoid a narrow
        # window during which teams can send memes
        meme_channel = get_text_channel_by_name(f'{config.EPISODES[episode_idx]}-memes', self.bot.guilds[0])
        await meme_channel.set_permissions(role, read_messages=True, send_messages=False)
        await category.set_permissions(role, read_messages=True)
        text_channel = get_text_channel_by_name(f'{config.EPISODES[episode_idx]}-finishers', self.bot.guilds[0])
        await text_channel.send(f'🥳 Congratulations {role.mention}!! 🎉')

    @commands.command(name='finish', help="(Admin Only) Add a team to the finishers role", hidden=True)
    async def finish(self, ctx, episode, team_role):
        team_role_id = "".join(filter(str.isdigit, team_role))
        guild = ctx.guild or self.bot.guilds[0]
        message = ctx.message
        user = message.author

        if await censor_message_if_public(ctx):
            return
        log.info("Team finished episode", team_role=team_role, episode=episode)

        if not await check_member_role(user, [config.ADMIN_ROLE], guild) and user != guild.owner:
            log.warning("Finish permission denied", user=user.name)
            await ctx.send("You need to be server owner or admin to use this command")
            return

        team_role = guild.get_role(int(team_role_id))
        await self.add_to_finishers(episode, team_role)
