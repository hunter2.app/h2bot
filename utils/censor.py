import discord
import structlog

from utils import config

log = structlog.get_logger(__name__)


async def censor_message_if_public(ctx):
    # Returns True if message was censored
    if isinstance(ctx.channel, discord.TextChannel) and (ctx.channel.name in config.PUBLIC_CHANNELS or ctx.channel.name in config.ANNOUNCE_CHANNELS):
        # Delete message if sent publicly (the announcement channels cannot be sent to, but this also prevents admins doing silly things)
        await ctx.message.delete()

        user = ctx.message.author
        log.warning("Censored message", user=user.name, channel=ctx.channel.name)
        await user.send(content="Please only use this command in a PM or team chats.")
        return True
    return False
