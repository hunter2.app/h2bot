def get_text_channel_by_name(channel, guild):
    chan_objects = list(chan for chan in guild.text_channels if chan.name == channel)

    if len(chan_objects) != 1:
        raise ValueError("Search for text channel did not return exactly 1 result")

    return chan_objects[0]
