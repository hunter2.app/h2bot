import logging

import discord


def get_role_by_name(role, guild):
    logging.debug("Searching for role with name {}".format(role))
    role_objects = [s_role for s_role in guild.roles if s_role.name == role]
    logging.debug("List of roles found: {}".format(role_objects))

    if len(role_objects) != 1:
        raise ValueError("Search for role did not return exactly 1 result")

    return role_objects[0]


async def ensure_role(role, guild):
    try:
        role_obj = get_role_by_name(role, guild)
    except ValueError:
        logging.info("Creating role {}".format(role))
        role_obj = await guild.create_role(name=role, hoist=True)

    return role_obj


def role_name_for_team(team_name):
    return "team-{}".format(team_name)[:100]


async def check_member_role(member_object, accepted_roles, guild):
    if isinstance(member_object, discord.User):
        member_object = await guild.fetch_member(member_object.id)

    for role in member_object.roles:
        if role.name in accepted_roles:
            return True
    return False
