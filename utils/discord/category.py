def get_category_by_name(category, guild):
    cat_objects = [cat for cat in guild.categories if cat.name == category]

    if len(cat_objects) != 1:
        raise ValueError("Search for text channel did not return exactly 1 result")

    return cat_objects[0]
