import os

LOGLEVEL = os.getenv('H2_LOGLEVEL') or "info"

DISCORD_ADMIN_ROLE = os.getenv('H2_DISCORD_ADMIN_ROLE') or "discord-admin"
ADMIN_ROLE = os.getenv('H2_ADMIN_ROLE') or "hunt-admin"
EXTRA_ROLES = os.getenv('H2_EXTRA_ROLES').split(",") if os.getenv('H2_EXTRA_ROLES') else []
ROLES_TO_CREATE = [DISCORD_ADMIN_ROLE, ADMIN_ROLE] + EXTRA_ROLES

MEME_CHANNEL = os.getenv('H2_MEME_CHANNEL') or "\U0001F921│memes"
ADMIN_MEME_CHANNEL = os.getenv('H2_ADMIN_MEME_CHANNEL') or "admin-meme"
ADMIN_STUCK_CHANNEL = os.getenv('H2_ADMIN_STUCK_CHANNEL') or "admin-stuck"
ADMIN_FEEDBACK_CHANNEL = os.getenv('H2_ADMIN_FEEDBACK_CHANNEL') or "admin-feedback"

PUBLIC_CATEGORY = os.getenv('H2_PUBLIC_CATEGORY') or "Public"
EXTRA_PUBLIC_CHANNELS = os.getenv('H2_EXTRA_PUBLIC_CHANNELS').split(",") if os.getenv('H2_EXTRA_PUBLIC_CHANNELS') else []
# Public channels will auto-censor "sensitive" commands
PUBLIC_CHANNELS = ["\U0001F4AC│general", "\U0001F93C│looking-for-team", "\U0001F192│advertising"] + EXTRA_PUBLIC_CHANNELS
EXTRA_PUBLIC_VOICE_CHANNELS = os.getenv('H2_EXTRA_PUBLIC_VOICE_CHANNELS').split(",") if os.getenv('H2_EXTRA_PUBLIC_VOICE_CHANNELS') else []
PUBLIC_VOICE_CHANNELS = ["Public Voice"] + EXTRA_PUBLIC_VOICE_CHANNELS
EXTRA_ANNOUNCE_CHANNELS = os.getenv('H2_EXTRA_ANNOUNCE_CHANNELS').split(",") if os.getenv('H2_EXTRA_ANNOUNCE_CHANNELS') else []
ANNOUNCE_CHANNELS = ["\U0001F44B│welcome", "\U0001F4E2│announcements", MEME_CHANNEL] + EXTRA_ANNOUNCE_CHANNELS

ADMIN_CATEGORY = os.getenv('H2_ADMIN_CATEGORY') or "admins"
EXTRA_ADMIN_CHANNELS = os.getenv('H2_EXTRA_ADMIN_CHANNELS').split(",") if os.getenv('H2_EXTRA_ADMIN_CHANNELS') else []
ADMIN_CHANNELS = ["hunt-admin-general", "hunt-admin-hunter2", "hunt-admin-puzzle-design",
                  "hunt-admin-finance", "hunt-admin-merch",
                  ADMIN_MEME_CHANNEL, ADMIN_STUCK_CHANNEL, ADMIN_FEEDBACK_CHANNEL] + EXTRA_ADMIN_CHANNELS
EXTRA_ADMIN_VOICE_CHANNELS = os.getenv('H2_EXTRA_ADMIN_VOICE_CHANNELS').split(",") if os.getenv('H2_EXTRA_ADMIN_VOICE_CHANNELS') else []
ADMIN_VOICE_CHANNELS = ["hunt-admin-voice"] + EXTRA_ADMIN_VOICE_CHANNELS

# This defines the finishers channels created
EPISODES = os.getenv('H2_EPISODES').split(",") if os.getenv('H2_EPISODES') else ["\U0001F389│episode1", "\U0001F973│episode2"]
EPISODE_IDS = [int(e) for e in os.getenv('H2_EPISODE_IDS').split(",")] if os.getenv('H2_EPISODE_IDS') else [1, 2]

EXTRA_CATEGORIES = os.getenv('H2_EXTRA_CATEGORIES').split(",") if os.getenv('H2_EXTRA_CATEGORIES') else []

# This defines any other manually created resources which should be ignored
IGNORE_CHANNELS = os.getenv('H2_IGNORE_CHANNELS').split(",") if os.getenv('H2_IGNORE_CHANNELS') else []
IGNORE_VOICE_CHANNELS = os.getenv('H2_IGNORE_VOICE_CHANNELS').split(",") if os.getenv('H2_IGNORE_VOICE_CHANNELS') else []
IGNORE_CATEGORIES = os.getenv('H2_IGNORE_CATEGORIES').split(",") if os.getenv('H2_IGNORE_CATEGORIES') else []
IGNORE_ROLES = os.getenv('H2_IGNORE_ROLES').split(",") if os.getenv('H2_IGNORE_ROLES') else []

PERMANENT_CHANNELS = ADMIN_CHANNELS + ANNOUNCE_CHANNELS + PUBLIC_CHANNELS + IGNORE_CHANNELS
PERMANENT_VOICE_CHANNELS = ADMIN_VOICE_CHANNELS + PUBLIC_VOICE_CHANNELS + IGNORE_VOICE_CHANNELS
PERMANENT_CATEGORIES = [ADMIN_CATEGORY, PUBLIC_CATEGORY] + EXTRA_CATEGORIES + IGNORE_CATEGORIES
PERMANENT_ROLES = ROLES_TO_CREATE + ["@everyone"] + IGNORE_ROLES

FEEDBACK_LINK = os.getenv('H2_FEEDBACK_LINK')

HUNTER2_TOKEN = os.getenv('H2_API_TOKEN')
HUNTER2_URL = os.getenv('H2_API_URL').strip('/')
TOKEN = os.getenv('H2_DISCORD_TOKEN')
